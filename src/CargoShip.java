/**
 * Created by Dmitry Vereykin on 7/23/2015.
 */
public class CargoShip extends CruiseShip {
    int maxCapacity;

    public CargoShip() {
        this.maxCapacity = 0;
    }

    public CargoShip(String name, String year,int maxPassengers, int maxCapacity) {
        super(name, year, maxPassengers);
        this.maxCapacity = maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public String toString() {
        //Since capacity is measured in TEU, I'll use DWT or how much it carry
        String str = "\nMaximum carrying weight in tonnes: " + this.maxCapacity;
        return super.toString() + str;
    }
}

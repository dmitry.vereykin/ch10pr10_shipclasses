/**
 * Created by Dmitry Vereykin on 7/22/2015.
 */
public class Ship {
    private String name;
    private String year;

    public Ship() {
        this.name = "";
        this.year = "";
    }

    public Ship(String name, String year) {
        this.name = name;
        this.year = year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public String getYear() {
        return year;
    }

    public String toString() {
        String str = "\nName of the ship: " + this.getName() + "\nYear: " + this.getYear();
        return str;
    }
}